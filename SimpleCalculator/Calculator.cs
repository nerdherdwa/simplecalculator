﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    class Calculator
    {
        private int _id;
        private string _answer;

        public void EnterCalculations()
        {
            string userChoice;
            userChoice = PromptUser();
            while (userChoice != "Q")
            {
                int value1 = PromptUserValues();
                int value2 = PromptUserValues();
                Calculate(userChoice, value1, value2);
                Console.Clear();
                PrintAnswer(_answer, userChoice, value1, value2);
                userChoice = PromptUser();
            }
        }

        private void PrintAnswer(string answer, string userChoice, int value1, int value2)
        {
            string calculateType = GetCalculationType(userChoice);
            Console.WriteLine("You are going to {0} a= {1} and b={2}", calculateType, value1, value2);
            Console.WriteLine();
            Console.WriteLine("The Answer is {0}", answer);
            Console.ReadLine();
            Console.Clear();           
        }

        private string GetCalculationType(string userChoice)
        {
            switch(userChoice)
            {
                case "A":
                    return "Add";
                case "S":
                    return "Subtract";
                case "M":
                    return "Multiply";
                default:
                    return "Divide";
            }
        }

        private string Calculate(string userChoice, int value1, int value2)
        {
            switch (userChoice)
            {
                case "A":
                    _answer = AddValues(value1, value2).ToString();
                    break;
                case "S":
                    _answer = SubtractValues(value1, value2).ToString();
                    break;
                case "M":
                    _answer = MultiplyValues(value1, value2).ToString();
                    break;
                default:
                    _answer = DivideValues(value1, value2).ToString();
                    break;
            }
            return _answer;
        }

        private double DivideValues(int value1, int value2)
        {
            return (double)value1/value2;
        }

        private int MultiplyValues(int value1, int value2)
        {
            return value1 * value2;
        }

        private int SubtractValues(int value1, int value2)
        {
            return value1 - value2;
        }

        private int AddValues(int value1, int value2)
        {
            return value1 + value2;
        }

        private int PromptUserValues()
        {
            while(true)
            {
                Console.Clear();
                Console.Write("Enter the value ");
                string userValue = Console.ReadLine();

                if(int.TryParse(userValue, out _id) == true )
                {
                    return _id;
                }
                else
                {
                    Console.WriteLine("Invlaid Value");
                    Console.ReadLine();
                }
            }
        }

        private string PromptUser()
        {
            while(true)
            {
                Console.Write("Enter the calculation type (A)dd, (S)ubtract, (M)ultiplication, (D)ivide or (Q)uit: ");
                string choice = Console.ReadLine().ToUpper();

                if (choice == "A" || choice == "S" || choice == "M" || choice == "D" || choice == "Q")
                    return choice;
                else
                    Console.WriteLine("Invalid Choice!");
            }
        }
    }
}
